import os
import os.path as osp
import sys
import subprocess
import torch
from torch.utils.data import DataLoader
import torchvision
import argparse
import yaml
import tqdm
import glob
import cv2
import time
import numpy as np
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
import codes.utils.util as util
from codes.data.EDVR_dataset import EDVRInferenceDataset
import codes.models.archs.EDVR_arch as EDVR_arch


MAX_RESOLUTION_SIZE = 1280 * 720

class EDVRWrapper:
    def __init__(self, logger, **conf):
        self.device = "cuda:%s" % str(conf['gpu_id']) if torch.cuda.is_available() else "cpu"

        self.ckpt_path = conf['ckpt_path']
        self.padding = conf['padding']
        self.batch = conf['batch']
        self.num_workers = conf['num_workers']
        self.split_H = conf['split_H']
        self.split_W = conf['split_W']
        self.network_conf = conf['network_conf']
        self.logger = logger

        self.logger.debug("conf device : {}".format(self.device))
        self.logger.debug("conf ckpt_path : {}".format(self.ckpt_path))
        self.logger.debug("conf padding : {}".format(self.padding))
        self.logger.debug("conf batch : {}".format(self.batch))
        self.logger.debug("conf num_workers : {}".format(self.num_workers))
        self.logger.debug("conf split_H : {}".format(self.split_H))
        self.logger.debug("conf split_W : {}".format(self.split_W))
        self.logger.debug("conf network_conf : {}".format(self.network_conf))


        with torch.no_grad():
            _t = time.time()
            self.logger.info("Load model...")
            self.model = EDVR_arch.EDVR(**self.network_conf)

            # set up the models
            self.model.load_state_dict(torch.load(self.ckpt_path, map_location=self.device), strict=True)
            self.model.to(self.device)
            self.model.eval()
            self.logger.info("Model Loaded")
            self.logger.debug("Model Load time : {}".format(time.time() - _t))

    def save_sr_video(self, input_path, output_path):
        if self.__verification_resolution(input_path, output_path):  # image resolution < 720p
            # read input_path Video info
            _, _, info = torchvision.io.read_video(input_path)
            video_fps = info['video_fps']

            # encoding and saving input video (vcodec "libx264", acodec "aac")
            _to_temp_path = os.path.splitext(input_path)[0]

            self.logger.debug("START SAFE ENCODING")
            _safe_encoding_time = time.time()
            pre_ffmpeg_command = ["ffmpeg", "-y",
                                  "-loglevel", "panic",
                                  "-i", input_path,
                                  "-vcodec", "libx264",
                                  "-acodec", "aac",
                                  "-pix_fmt", "yuv420p",
                                  "-src_range", "1",
                                  "-dst_range", "1",
                                  "-strict", "-2",
                                  "-crf", "23",
                                  "-r", str(int(video_fps)),
                                  "{}_temp.mp4".format(_to_temp_path)]
            subprocess.call(" ".join(pre_ffmpeg_command), shell=True)
            subprocess.call("mv {}_temp.mp4 {}".format(_to_temp_path, input_path), shell=True)
            self.logger.debug("FINISHED SAFE ENCODING {}".format(time.time() - _safe_encoding_time))

            # create Dataset and Dataloader
            self.logger.debug("START Create Dataset and Dataloader")
            _dataset_time = time.time()
            infer_dataset = EDVRInferenceDataset(input_path, self.network_conf['nframes'], padding='replicate')
            infer_loader = DataLoader(infer_dataset, batch_size=self.batch, shuffle=False, num_workers=self.num_workers)
            self.logger.debug("Created Dataset and Dataloader : {}".format(time.time() - _dataset_time))

            self.logger.debug("START INFERENCE")
            _inference_time = time.time()
            # process each image
            output_tensor = list()
            with torch.no_grad():
                for input_tensor in tqdm.tqdm(infer_loader):
                    output = self.single_inference(input_tensor.to(self.device))
                    output_tensor.append(output)
                    del output

            output_tensor = torch.cat(output_tensor, dim=0)  # output_tensor: Tensor[T,C,H,W]

            # Tensor to numpy
            output_tensor = output_tensor.squeeze().float().cpu().clamp_(0, 1)  # clamp
            output = output_tensor.numpy()
            del output_tensor
            output = np.transpose(output[:, [2, 1, 0], :, :], (0, 2, 3, 1))  # THWC, BGR
            output = (output * 255.0).round().astype(np.uint8)

            # write video
            video_writer = cv2.VideoWriter(
                output_path, cv2.VideoWriter_fourcc(*'mp4v'), video_fps, (output.shape[2], output.shape[1])
            )
            for i in range(output.shape[0]):
                video_writer.write(output[i])
            video_writer.release()
            del output
            self.logger.debug("FINISHED INFERENCE DONE! time : {}".format(time.time() - _inference_time))

            self.logger.debug("START LAST ENCODING")
            _last_encoding_time = time.time()
            post_ffmpeg_command = ["ffmpeg", "-y",
                                   "-loglevel", "panic",
                                   "-i", output_path,
                                   "-i", input_path,
                                   "-map", "0:v",
                                   "-map", "1:a",
                                   "-vcodec", "libx264",
                                   "-acodec", "aac",
                                   "-strict", "-2",
                                   "-crf", "23",
                                   "-r", str(int(video_fps)),
                                   "{}_temp.mp4".format(output_path)]

            subprocess.call(" ".join(post_ffmpeg_command), shell=True)
            subprocess.call("mv {}_temp.mp4 {}".format(output_path, output_path), shell=True)

            self.logger.debug("FINISHED LAST Encoding : {}".format(time.time() - _last_encoding_time))

    def single_inference(self, input_tensor):
        h_outputs = list()
        h_tensors = input_tensor.split(self.split_H, dim=-2)
        for h_tensor in h_tensors:
            w_outputs = list()
            split_tensors = h_tensor.split(self.split_W, dim=-1)
            for split_tensor in split_tensors:
                output = util.single_forward(self.model, split_tensor)  # output: Tensor[B,1,C,H,W]
                output = output.squeeze(1)  # output: Tensor[B,C,H,W]
                w_outputs.append(output)
            h_outputs.append(torch.cat(w_outputs, dim=-1))
        result = torch.cat(h_outputs, dim=-2)
        return result

    def __verification_resolution(self, video_path, output_path):
        self.logger.info("Check Input Resolution")

        resolution_command = [
            "ffprobe", "-loglevel",
            "quiet", "-select_streams",
            "v:0", "-show_entries",
            "stream=width,height",
            "-of", "csv=p=0",
            video_path
        ]
        resolution_info = subprocess.check_output(" ".join(resolution_command), shell=True)
        w, h = resolution_info.decode('utf-8').replace('\n', '').split(',')
        if int(w) * int(h) <= MAX_RESOLUTION_SIZE:  # 1280 * 720 (720p)
            self.logger.debug("resolution <= 720p")
            self.logger.info("Checked")
            return True
        else:
            self.logger.debug("resolution > 720p. \n Skip {} Inference".format(video_path))
            self.logger.info("Checked")
            subprocess.call("cp {} {}".format(video_path, output_path), shell=True)
            return False


def get_output_path(input_path, output_path=None):
    if output_path is None:
        dir_, base = osp.split(input_path)
        filename, ext = osp.splitext(base)
        output_path = os.path.join(dir_, '{}_output{}'.format(filename, ext))
    else:
        output_path = output_path
    return output_path


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for config.")
    parser.add_argument('-i', '--input', type=str, required=True,
                        help="path to input video file (Unix style pathname pattern expansion)")
    parser.add_argument('-o', '--output', type=str, default=None,
                        help="path to output video file")
    args = parser.parse_args()

    with open(args.config) as f:
        conf = yaml.load(f, Loader=yaml.Loader)
    wrapper = EDVRWrapper(**conf)
    input_path_list = glob.glob(args.input, recursive=True)
    if len(input_path_list) is 0:
        raise ValueError('Input is not specified')
    elif len(input_path_list) is 1:
        output_path = get_output_path(input_path_list[0], args.output)
        wrapper.save_sr_video(input_path_list[0], output_path)
    else:
        for input_path in input_path_list:
            output_path = get_output_path(input_path)
            wrapper.save_sr_video(input_path, output_path)


if __name__ == "__main__":
    main()
