import torch
from torch.utils.data import Dataset
import cv2
import numpy as np
from .util import index_generation


class EDVRInferenceDataset(torch.utils.data.Dataset):
    """EDVR_inference_dataset"""
    def __init__(self, video_path, network_nframes=5, padding='replicate'):
        self.imgs_LQ = self._get_video_frames(video_path)
        self.max_idx = self.imgs_LQ.shape[0]
        self.padding = padding
        self.nframes = network_nframes
        self.frame_batches = self._get_adjusted_frames(self.imgs_LQ, self.max_idx)

    def _get_video_frames(self, video_path):
        # read LQ images
        video_cap = cv2.VideoCapture(video_path)
        imgs_LQ = list()
        while video_cap.isOpened():
            ret, frame = video_cap.read()
            if ret:
                frame = frame.astype(np.float32) / 255.
                imgs_LQ.append(frame)
            else:
                break
        video_cap.release()

        # numpy to torch
        imgs_LQ = np.stack(imgs_LQ, axis=0)  # imgs_LQ: numpy[T,H,W,C]
        imgs_LQ = imgs_LQ[:, :, :, [2, 1, 0]]  # BGR -> RGB
        imgs_LQ = torch.from_numpy(
            np.ascontiguousarray(np.transpose(imgs_LQ, (0, 3, 1, 2)))
        ).float()  # imgs_LQ: Tensor[T,C,H,W]

        # cut border and get max_frame idx
        _, _, height, width = imgs_LQ.shape
        imgs_LQ = imgs_LQ[:, :, :height - (height % 16), :width - (width % 16)]

        return imgs_LQ

    def _get_adjusted_frames(self, imgs_LQ, max_idx):
        batch = list()
        for img_idx in range(max_idx):
            select_idx = index_generation(img_idx, max_idx, self.nframes, padding=self.padding)
            # imgs_in: Tensor[nframes,C,H,W]
            batch.append(imgs_LQ.index_select(0, torch.LongTensor(select_idx)))

        batch_tensor = torch.stack(batch, dim=0)  # Tensor[B,nframes,C,H,W]

        return batch_tensor

    def __len__(self):
        return len(self.frame_batches)

    def __getitem__(self, idx):
        return self.frame_batches[idx, :, :, :, :]