# -*- coding: utf-8 -*-

import os
import sys
import time
import uuid
import yaml
import grpc
from datetime import datetime
from concurrent import futures
import logging
import argparse

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))) + "/codes")
from grpc_server_util import *

from GRPC_server.grpc_utils import save_chunks_to_file, get_file_chunks, get_rest_timeout, get_max_time_interval
from GRPC_server.resolution_pb2 import ProcessStatus, UploadStatus
from GRPC_server.resolution_pb2_grpc import SuperResolutionGRPCServicer, add_SuperResolutionGRPCServicer_to_server
from codes.EDVR_inference import EDVRWrapper
from codes.utils.util import setup_logger

job_board = JobStatusCheck()


class SuperResolutionGRPC(SuperResolutionGRPCServicer):
    def __init__(self, logger):
        super(SuperResolutionGRPCServicer, self).__init__()
        self.logger = logger

    def Upload(self, request_iterator, context):
        randkey = str(uuid.uuid4()) + ".mp4"
        save_chunks_to_file(request_iterator, "server_tmp_files/input/" + randkey, logger=self.logger)

        job_board.add_job_element(randkey)
        now_string = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        job_board.update_element(randkey, column_key='upload_at', value=now_string)
        return UploadStatus(status=1, file_key=randkey, error_msg="")

    def CheckProcess(self, request, context):
        find_filekey = request.file_key
        job_elem = job_board.get_job_element(find_filekey)

        if job_elem is None:
            return ProcessStatus(status=9, message="File is deleted (old file key)")
        else:
            if job_elem["act_at"] is None:
                return ProcessStatus(status=6, message="Process is in queue")
            elif job_elem["act_at"] == "Processing":
                return ProcessStatus(status=7, message="The file is now processing")
            elif job_elem["act_at"] == "Error":
                return ProcessStatus(status=11, message="File Processing Error")
            else:
                self.logger.debug("CheckProcess : {}".format(job_elem["act_at"]))
                return ProcessStatus(status=8, message="Process completed (can download)")

    def Download(self, request, context):
        return get_file_chunks("server_tmp_files/output/" + request.file_key)


class FileServer(SuperResolutionGRPCServicer):
    def __init__(self, logger, max_workers=10, config_path="/workspace/mmsr/codes/options/test/EDVR_inference.yml"):
        super(FileServer, self).__init__()
        initial_dir()
        self.logger = logger
        with open(config_path) as f:
            self.config = yaml.load(f, Loader=yaml.Loader)
        self.video_sr_model = EDVRWrapper(self.logger, **self.config)
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=max_workers))
        add_SuperResolutionGRPCServicer_to_server(SuperResolutionGRPC(self.logger), self.server)

    def process_overall(self, job_board, job_todo):
        find_proc_file(self.video_sr_model, job_board, job_todo["job_key"], logger=self.logger)

    def start(self, ip, port):
        self.logger.info("server started >> {} : {}".format(ip, port))
        self.server.add_insecure_port(f'[::]:{port}')
        self.server.start()

        try:
            while True:
                job_todo = None  # checking for whether finding next job
                now_string = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

                # job_board에 있는 모든 job들 훑어보기
                remove_jobs = list()
                for request_key, job_elem in job_board.job_scheduler.items():
                    waiting_time = get_time_difference(now_string, job_elem["upload_at"])

                    # REST TIMEOUT이 된 Job들에 대해서는 job_board에서 지워도 됨
                    if waiting_time >= get_rest_timeout() and job_elem["act_at"] is None:
                        remove_jobs.append(request_key)

                    if waiting_time >= get_max_time_interval() and job_elem["act_at"] is not None:
                        remove_jobs.append(request_key)

                    if waiting_time >= get_max_time_interval() and job_elem["act_at"] == "Error":
                        remove_jobs.append(request_key)

                    # REST TIMEOUT이 되지 않은 Job들인데 아직 진행되지 않은 걸 발견했을 때
                    elif waiting_time < get_rest_timeout() and job_elem["act_at"] is None and not job_todo:
                        self.logger.info("INPUT  -> {:s}".format("server_tmp_files/input/" + request_key))
                        self.logger.info("OUTPUT -> {:s}".format("server_tmp_files/output/" + request_key))

                        self.logger.debug("waiting time: {:.2f}".format(waiting_time))
                        self.logger.debug("Get job for processing")

                        job_todo = {"job_key": request_key, "job_elem": job_elem}
                        self.logger.debug("job_todo : {}".format(job_todo))
                        break

                for _request_key in remove_jobs:
                    job_board.remove_job_element(_request_key)

                # 해야 할 job을 찾았을 때 실행
                if job_todo is not None:
                    # Model Inference & 진행한 job에 대한 결과 기록
                    try:
                        self.logger.debug("START VIDEO SR INFERENCE")
                        _start_infer = time.time()
                        self.process_overall(job_board, job_todo)
                        job_board.update_element(job_todo["job_key"], column_key="act_at",
                                                 value=datetime.fromtimestamp(time.time()).strftime(
                                                     '%Y-%m-%d %H:%M:%S'))
                        self.logger.debug("INFERENCE DONE! {}".format(time.time() - _start_infer))
                        # job_todo DONE!

                    except Exception as e:
                        self.logger.error('An error : {}, occurred during the reference.\n'
                                          'job key : {}'.format(e, job_todo['job_key']))
                        job_board.update_element(job_todo["job_key"], column_key="act_at", value="Error")
                        # job_todo ERROR!

                    self.logger.debug("Finish writing result path")

                else:
                    self.logger.info(str(job_board))
                time.sleep(3)

        except KeyboardInterrupt:
            self.logger.error("KeyboardInterrupt. Server STOP")
            self.server.stop(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Video SR server')
    parser.add_argument('--ip',
                        help='server ip address. (default:0.0.0.0)',
                        type=str,
                        default='0.0.0.0')
    parser.add_argument('--port',
                        help='server port. (default:50051)',
                        type=int,
                        default=50051)
    parser.add_argument('--log_level',
                        type=str,
                        help='logger level (default: INFO)',
                        default='INFO')
    args = parser.parse_args()

    logger = setup_logger(logger_name='MAUM_VideoSR', level=args.log_level.upper())
    server = FileServer(logger)
    server.start(args.ip, args.port)
