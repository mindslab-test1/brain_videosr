from GRPC_server.resolution_pb2 import FileChunk

CHUNK_SIZE = 1024 * 1024  # 1MB
REST_TIMEOUT = 60 * 10  # 600 sec
MAX_TIME_INTERVAL = 60 * 60 * 24  # 1 day

def get_file_chunks(filename):
    with open(filename, 'rb') as f:
        while True:
            piece = f.read(CHUNK_SIZE)
            if len(piece) == 0:
                return
            yield FileChunk(file_bytes=piece)


def save_chunks_to_file(chunks, filename, logger=None):
    if logger is not None:
        logger.info("Saved file in client_tmp -> " + filename)
    with open(filename, 'wb') as f:
        for chunk in chunks:
            f.write(chunk.file_bytes)


def get_chunk_size():
    return CHUNK_SIZE


def get_rest_timeout():
    return REST_TIMEOUT


def get_max_time_interval():
    return MAX_TIME_INTERVAL
