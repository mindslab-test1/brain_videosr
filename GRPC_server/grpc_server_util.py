import os
from datetime import datetime
import sys
import threading

job_class_lock = threading.RLock()
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from codes.EDVR_inference import get_output_path


def find_proc_file(model, job_board, job_key, logger):
    now_process_status = job_board.get_job_element(str(job_key), column_key="act_at")
    if now_process_status is None:
        job_board.update_element(str(job_key), column_key="act_at", value="Processing")
    logger.info("Processing in " + job_key)

    # Processing Block
    input_path = 'server_tmp_files/input/{}'.format(job_key)
    output_path = get_output_path(input_path, 'server_tmp_files/output/{}'.format(job_key))
    model.save_sr_video(input_path, output_path)


def initial_dir():
    if not (os.path.isdir("server_tmp_files")):
        os.makedirs(os.path.join("server_tmp_files"))
    if not (os.path.isdir("server_tmp_files/input")):
        os.makedirs(os.path.join("server_tmp_files/input"))
    if not (os.path.isdir("server_tmp_files/output")):
        os.makedirs(os.path.join("server_tmp_files/output"))


def get_time_difference(strftime_now, strftime_old):
    FMT = '%Y-%m-%d %H:%M:%S'
    tdelta = datetime.strptime(strftime_now, FMT) - datetime.strptime(strftime_old, FMT)
    return tdelta.total_seconds()


def delete_tmp_file(job_key):
    try:
        os.remove('server_tmp_files/input/{}'.format(job_key))
    except:
        pass
    try:
        os.remove('server_tmp_files/output/{}'.format(job_key))
    except:
        pass


class JobStatusCheck:
    def __init__(self):
        self.job_scheduler = dict()

    def add_job_element(self, request_key, return_val=True):
        with job_class_lock:
            self.job_scheduler[request_key] = {"upload_at": None, "act_at": None, "deleted_at": None}
            if return_val:
                return self.job_scheduler[request_key]

    def get_job_element(self, request_key, column_key=None):
        with job_class_lock:
            try:
                job_elem = self.job_scheduler[request_key]
            except KeyError:
                return None

            if column_key is not None:
                return job_elem[column_key]
            else:
                return job_elem

    def remove_job_element(self, request_key, return_val=True):
        with job_class_lock:
            try:
                popped_elem = self.job_scheduler.pop(request_key)
                delete_tmp_file(job_key=request_key)
                if return_val:
                    return popped_elem

            except KeyError:
                return None

    def update_element(self, request_key, column_key, value):
        with job_class_lock:
            self.job_scheduler[request_key][column_key] = value

    def is_exist(self, request_key):
        with job_class_lock:
            try:
                job_elem = self.job_scheduler[request_key]
            except KeyError:
                return False

            if job_elem:
                return True

    def __str__(self):
        if len(self.job_scheduler) != 0:
            return "\t".join(["{:s}...{:s}".format(key, status) for key, status in
                              zip(self.job_scheduler.keys(), [x['act_at'] for x in self.job_scheduler.values()])])
        return "No Process"
