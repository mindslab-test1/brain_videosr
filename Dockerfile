FROM pytorch/pytorch:1.2-cuda10.0-cudnn7-devel

COPY . /workspace/mmsr
WORKDIR /workspace/mmsr
RUN cd /workspace/mmsr/codes/models/archs/dcn && CUDAHOSTCXX=/usr/bin/gcc-5 python setup.py develop
RUN apt-get update && apt-get -y upgrade && apt-get -y install libglib2.0-0 libsm6 libxrender-dev libxext6 ffmpeg
RUN cd /workspace/mmsr
COPY ./requirements.txt ./
RUN python3 -m pip install --no-cache-dir -r requirements.txt
RUN python3 -m pip install grpcio==1.33.1 --ignore-installed
RUN python3 -m pip install grpc-tools==0.0.1
RUN python3 -m pip install opencv-contrib-python==4.2.0.34
# set TORCH_CUDA_ARCH_LIST properly. (6, 1) -> 6.1
ARG TORCH_CUDA_ARCH_LIST=7.0
ENV DEVICE_ID=0
ENV IP=0.0.0.0
ENV PORT=50051
EXPOSE 50051
ENV LOG_LEVEL=INFO
ENTRYPOINT sh run.sh ${DEVICE_ID} ${IP} ${PORT} ${LOG_LEVEL}