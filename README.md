# MMSR

MMSR is an open source image and video super-resolution toolbox based on PyTorch. It is a part of the [open-mmlab](https://github.com/open-mmlab) project developed by [Multimedia Laboratory, CUHK](http://mmlab.ie.cuhk.edu.hk). MMSR is based on our previous projects: [BasicSR](https://github.com/xinntao/BasicSR), [ESRGAN](https://github.com/xinntao/ESRGAN), and [EDVR](https://github.com/xinntao/EDVR).

### Highlights
- **A unified framework** suitable for image and video super-resolution tasks. It is also easy to adapt to other restoration tasks, e.g., deblurring, denoising, etc.
- **State of the art**: It includes several winning methods in competitions: such as ESRGAN (PIRM18), EDVR (NTIRE19).
- **Easy to extend**: It is easy to try new research ideas based on the code base.


### Updates
[2019-07-25] MMSR v0.1 is released.

## Dependencies and Installation

- Python 3 (Recommend to use [Anaconda](https://www.anaconda.com/download))
- [PyTorch >= 1.1](https://pytorch.org)
- NVIDIA GPU + [CUDA](https://developer.nvidia.com/cuda-downloads)
- [Deformable Convolution](https://arxiv.org/abs/1703.06211). We use [mmdetection](https://github.com/open-mmlab/mmdetection)'s dcn implementation. Please first compile it.
  ```
  cd ./codes/models/archs/dcn
  python setup.py develop
  ```
- Python packages: `pip install -r requirements.txt`

### Dockerfile

- dcn compilation has dependencies about GPU. So you should set `TORCH_CUDA_ARCH_LIST` in Dockerfile properly or passing proper value when build dockerfile like `docker build --build-arg TORCH_CUDA_ARCH_LIST=6.1 . `. You can know this value by `torch.cuda.get_device_capability()` in your machine.
- In other way, you can just remove build folder and recompile dcn in docker container.
  ```
  rm -rf ./codes/models/archs/dcn/build
  cd ./codes/models/archs/dcn
  python setup.py develop
  ```
- Docker build & run
  ```
  docker build -f Dockerfile -t  $IMG_NAME:$TAG .
  docker run -itd -v $SERVER_TMP_COPY_PATH:/workspace/mmsr/server_tmp_files -p 50051:50051 --gpus all -e IP=0.0.0.0 -e PORT=50051 -e DEVICE_ID=0 -e LOG_LEVEL=INFO --name $CONTAINER_NAME $IMAGE_NAME:$TAG
  ```

- at 114.108.173.113
  ```
  docker pull docker.maum.ai:443/brain/video_sr:v1.3
  docker run -it -v /DATA/video_sr/server_tmp_files:/workspace/mmsr/server_tmp_files -p 50051:50051 --gpus all -e IP=0.0.0.0 -e PORT=50051 -e DEVICE_ID=0 -e LOG_LEVEL=INFO --name brain_video_sr docker.maum.ai:443/brain/video_sr:v1.3
  ```


## Dataset Preparation
We use datasets in LDMB format for faster IO speed. Please refer to [DATASETS.md](datasets/DATASETS.md) for more details.

## Training and Testing
Please see [wiki- Training and Testing](https://github.com/open-mmlab/mmsr/wiki/Training-and-Testing) for the basic usage, *i.e.,* training and testing.

## Usage
Inference
```
docker exec -it $CON_NAME python codes/EDVR_inference.py -c codes/options/test/EDVR_inference.yml -i $INPUT_PATH -o $OUTPUT_PATH
```

Server
```
docker exec -it $CON_NAME python /workspace/mmsr/GRPC_server/grpc_server.py
```

Client
```
docker exec -it $CON_NAME python /workspace/mmsr/GRPC_server/grpc_client.py
```

## Model Zoo and Baselines
Results and pre-trained models are available in the [wiki-Model Zoo](https://github.com/open-mmlab/mmsr/wiki/Model-Zoo).

## Contributing
We appreciate all contributions. Please refer to [mmdetection](https://github.com/open-mmlab/mmdetection/blob/master/CONTRIBUTING.md) for contributing guideline.

**Python code style**<br/>
We adopt [PEP8](https://python.org/dev/peps/pep-0008) as the preferred code style. We use [flake8](http://flake8.pycqa.org/en/latest) as the linter and [yapf](https://github.com/google/yapf) as the formatter. Please upgrade to the latest yapf (>=0.27.0) and refer to the [yapf configuration](.style.yapf) and [flake8 configuration](.flake8).

> Before you create a PR, make sure that your code lints and is formatted by yapf.

## License
This project is released under the Apache 2.0 license.
